﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Project.WPF.Controls
{
    public class Lattice : Control
    {
        public Pen? RodPen { get; set; }

        private Canvas canvas { get; set; }

        public Lattice(Pen pen, Canvas canvas)
        {
            RodPen = pen;
            this.canvas = canvas;
        }

        public void OnRender(object sender, EventArgs e)
        {
            canvas.Children.Clear();

            const double maxCellSize = 50.0; // длина стороны 1-й клетки
            const int splitCnt = 2; // кол-во клеток, на которое будет производиться деление
            const double splitTreshold = 6.0; // порог расщепления (так просто не объяснить)

            if (RodPen is null) return;

            var side = Math.Max(canvas.ActualWidth, canvas.ActualHeight);
            var cellSide = side / splitTreshold;

            for (; cellSide >= maxCellSize; cellSide /= splitCnt) { }

            var horRodCnt = (int)(canvas.ActualWidth / cellSide) + 1;
            var verRodCnt = (int)(canvas.ActualHeight / cellSide) + 1;

            for (var i = 1; i < horRodCnt; i++)
            {
                var offsetX = i * cellSide;

                ShapeSettings.MakeLine(canvas, RodPen, new Point(offsetX, 0), new Point(offsetX, canvas.ActualHeight));
            }
            for (var i = 1; i < verRodCnt; i++)
            {
                var offsetY = i * cellSide;

                ShapeSettings.MakeLine(canvas, RodPen, new Point(0, offsetY), new Point(canvas.ActualWidth, offsetY));
            }
        }
    }
}