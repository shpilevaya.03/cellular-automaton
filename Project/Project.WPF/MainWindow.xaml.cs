﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Project.WPF.Controls;

namespace Project.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //<Pen Thickness="0.5" Brush="#FF545454"/>

            var lattice = new Lattice(
                new Pen()
                {
                    Brush = (Brush)new BrushConverter().ConvertFrom("#FF545454"),
                    Thickness = 0.5
                },
                MainCanvas
                );
            CompositionTarget.Rendering += lattice.OnRender;
        }
    }
}
