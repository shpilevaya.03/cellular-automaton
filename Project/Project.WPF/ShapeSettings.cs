﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Project.WPF
{
    public static class ShapeSettings
    {
        public static Rectangle MakeRectangle(Canvas canvas, Point location, Size size, Brush background, Brush pen)
        {
            var rect = new Rectangle() { RenderSize = size, Stroke = pen, Fill = background };

            Canvas.SetLeft(rect, location.X);
            Canvas.SetTop(rect, location.Y);
            canvas.Children.Add(rect);

            return rect;
        }

        public static Line MakeLine(Canvas canvas, Pen pen, Point point0, Point point1)
        {
            var line = new Line()
            {
                X1 = point0.X,
                X2 = point1.X,
                Y1 = point0.Y,
                Y2 = point1.Y,
                Stroke = pen.Brush,
                StrokeThickness = pen.Thickness
            };

            canvas.Children.Add(line);

            return line;
        }
    }
}
