﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public abstract class CellRepo
    {
        public abstract Cell[,] Cells { get; }
        public abstract int[,] Sums { get; }

        public int FindNeighborhoodSum((int left, int rigth) coordinates)
        {
            int count = 0;
            for (int i = coordinates.left - 1; i <= coordinates.left + 1; i++)
            {
                for (int j = coordinates.rigth - 1; j <= coordinates.rigth + 1; j++)
                {
                    if (coordinates.left == i && coordinates.rigth == j) continue;
                    if (i > -1 && i < Cells.GetLength(0) && j > -1 && j < Cells.GetLength(1) && Cells[i, j].isAlive)
                        count++;
                }
            }

            return count;
        }

        public abstract void Initialize();

        public void UpdateCellStates()
        {
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Cells[i, j].SetCellState(Sums[i, j]);
                }
            }
        }

        public void UpdateSums()
        {
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Sums[i, j] = FindNeighborhoodSum((i, j));
                }
            }
        }
    }
}
