﻿namespace Project.Core
{
    public class Cell
    {
        public bool isAlive        { get; private set; }

        public Cell(bool isAlive)
        {
            this.isAlive =      isAlive;
        }

        public void SetCellState(int sum)
        {
            if (isAlive && (sum < 2 || sum > 3))    isAlive = false;
            else if (!isAlive && sum == 3)          isAlive = true;
        }
    }
}