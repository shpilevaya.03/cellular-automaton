﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public struct Color
    {
        public const ConsoleColor HighlightColor = ConsoleColor.DarkCyan;
        public const ConsoleColor BaseColor = ConsoleColor.Black;
    }

    static class ConsoleHelper
    {
        public static void ClearScreen()
        {
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.Write(new string(' ', Console.WindowWidth), Console.BackgroundColor = Color.BaseColor);
                Console.SetCursorPosition(0, i);
            }
            Console.SetCursorPosition(0, 0);
        }

        public static void PrintHighlightedText(string context)
        {
            Console.WriteLine(context, Console.BackgroundColor = Color.HighlightColor);
            Console.BackgroundColor = Color.BaseColor;
        }
    }
}
