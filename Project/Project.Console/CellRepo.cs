﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;

namespace Project
{
    internal class CellRepoConsole : CellRepo, ICellRepo
    {
        public override Cell[,] Cells { get; }
        public override int[,] Sums { get; }

        readonly Random _rand = new Random();

        public CellRepoConsole(int lines, int columns)
        {
            Cells = new Cell[lines, columns];
            Sums =  new int[lines, columns];

            Initialize();
        }

        public void DrawGrid()
        {
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Console.Write(Cells[i, j].isAlive ? "■ " : "  ");
                }

                Console.WriteLine();
            }

            UpdateCellStates();
            UpdateSums();
        }

        public override void Initialize()
        {
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Cells[i, j] = new Cell(Convert.ToBoolean(_rand.Next(0, 2)));
                }
            }

            UpdateSums();
        }
    }
}
