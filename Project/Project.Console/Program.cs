﻿using System;

namespace Project
{
    class Program
    {
        public static void Main(string[] args)
        {
            CellRepoConsole repo = new CellRepoConsole(35, 35);
            int timeDelay = 100;

            while (true)
            {
                ConsoleHelper.ClearScreen();
                repo.DrawGrid();

                Thread.Sleep(timeDelay);
            }
        }
    }
}